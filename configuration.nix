# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      ./modules/bash.nix
      ./modules/console.nix
      ./modules/fnkeys.nix
      ./modules/i18n.nix
      ./modules/network.nix
      ./modules/pkgs.nix
      ./modules/pw.nix
      ./modules/sway.nix
      ./modules/users.nix
      ./modules/xorg.nix
      ./modules/zsh.nix
    ];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi = {
    canTouchEfiVariables = true;
  };
  boot.loader.grub.configurationLimit = 5;
  boot.kernelPackages = pkgs.linuxPackages_latest;
  boot.extraModulePackages = [
    pkgs.linuxPackages_latest.acpi_call
    pkgs.linuxPackages_latest-libre.cpupower
  ];
  boot.initrd.availableKernelModules = [
    "nvme" "xhci_pci" "btrfs" "ahci"
  ];
  # Set your time zone.
  time.timeZone = "Asia/Shanghai";



  # Enable CUPS to print documents.
  # services.printing.enable = true;

  # Enable sound.
  sound.enable = true;
  hardware.pulseaudio.enable = true;

  # pkg bin cache
  nix.binaryCaches = [
    "https://mirror.sjtu.edu.cn/nix-channels/store"
    "https://mirrors.ustc.edu.cn/nix-channels/store"
    "https://mirrors.tuna.tsinghua.edu.cn/nix-channels/store"
  ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };


  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "21.05"; # Did you read the comment?

}
