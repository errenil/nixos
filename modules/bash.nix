{ pkgs, config, ... }:
{
	programs.bash = {
		enableCompletion = true;
		#undistractMe.enable = true;
		shellAliases = {
			"e" = "emacs -nw";
			"ec"= "emacsclient -nw";
      "mg"= "mg -n";
		};
	};
}
