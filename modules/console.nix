{ pkgs, ...}:
{
  console = {
    font = "ter-124b";
    keyMap = "us";
    packages = with pkgs; [ terminus_font kbd ];
    colors = [
	# normal
	"1d2021"  # 0 black 
	"cc2414"  # 1 red
	"98991a"  # 2 green
	"d89a22"  # 3 yellow
	"468589"  # 4 blue
	"b16286"  # 5 purple
	"689e6a"  # 6 aqua/cyan
	"a99a84"  # 7 gray

	# bright
	"938374"  # 8 black
	"fc4935"  # 9 red
	"b8ba26"  # 10 green
	"fbbe2f"  # 11 yellow
	"83a699"  # 12 blue
	"d4869c"  # 13 purple
	"8fc17d"  # 14 aqua/cyan 
	"eddbb7"  # 15 white
    ];
  };
}
