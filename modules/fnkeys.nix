{ pkgs, ... }:
{
    services.actkbd = {
        enable = true;
        bindings = [
        # volume 59/60/60
        { keys = [ 59 ]; events = [ "key" ]; command = "${pkgs.alsaUtils}/bin/amixer -q set Master toggle"; }
        { keys = [ 60 ]; events = [ "key" ]; command = "${pkgs.alsaUtils}/bin/amixer -q set Master 2%-"; }
        { keys = [ 61 ]; events = [ "key" ]; command = "${pkgs.alsaUtils}/bin/amixer -q set Master 2%+"; }
        # brightness 63/64
        { keys = [ 63 ]; events = [ "key" ]; command = "${pkgs.brightnessctl}/bin/brightnessctl -d intel_backlight s 5%-"; }
        { keys = [ 64 ]; events = [ "key" ]; command = "${pkgs.brightnessctl}/bin/brightnessctl -d intel_backlight s +5%"; }
        ];
    };
}
