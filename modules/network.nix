{ pkgs, config, ... }:

{
  networking.hostName = "near"; # Define your hostname.
  networking.wireless = {
    interfaces = [ "wlp0s20f3" ]; 
    enable = true;  # Enables wireless support via wpa_supplicant.
  };
  networking.supplicant = {
    "wlp0s20f3" = {
        configFile.path = "/etc/wpa_supplicant.conf";
    };
  };

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  networking.useDHCP = false;
  networking.interfaces.wlp0s20f3.useDHCP = true;
  networking.interfaces.wwp0s20f0u2i1.useDHCP = true;

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;
}
