# system-wide pkgs and fonts
{ pkgs, config, ... }:
let nixos21 = import <nixos-21.11> { config = {allowUnfree = true; }; };
    # nixpkgs is alias for nixos-unstable channel ...
    unstable = import <nixpkgs> {config = {allowUnfree = true;};};
in {
  # system-wide config
  environment = {
	etc = {
            # put config files to /etc
            "tmux.conf".source = ../dotfiles/tmux/tmux.conf;
            "gitconfig".source = ../dotfiles/git/gitconfig;
            "xdg/awesome/rc.lua".source = ../dotfiles/awesome/rc.lua;
            "X11/xorg.conf.d/90-monitor.conf".source = ../dotfiles/x11/90-monitor.conf;
	};
	  # List packages installed in system profile. To search, run:
	  # $ nix search wget
        systemPackages = with pkgs; [
            # cli
            acpi
            acpid
            acpilight
            acpitool

            aspell
            aspellDicts.en
            aspellDicts.en-science
            aspellDicts.en-computers

            autoconf
            automake
            awesome
            bspwm
            brightnessctl
            cmake
            dbus
            dhcpcd
            feh
            figlet
            fontconfig
            fzf

            clojure
            jdk17_headless

            gcc11	# 11.1.0
            geoclue2
            git
            gnumake
            go_1_17		  # 1.17.5
            grim        # screenshot for Wayland

            htop
            hikari

            i7z         # a better i7 (and i3/i5) reporting tool

            lf
            light
            lua5_3
            lua53Packages.luarocks

            neofetch
            nix-prefetch-scripts

            openconnect

            pkg-config
            python39
            p7zip
            pulseaudio
            nixos21.postgresql_14

            redshift
            ripgrep
            rxvt-unicode

            scrot           # screenshot for X
            smartmontools
            sxhkd
            tmux
            tokei
            tdesktop        # telegram-desktop
            upower
            tlp
            powertop
            unzip

            wezterm
            wget
            wpa_supplicant

            picom           # Compositor for X
            xclip
            xorg.libX11
            xorg.xdpyinfo   # X DPI detector
            xorg.xev
            xbindkeys


            zathura
            zip
            zlib
            z-lua
            unstable.zprint
	    unstable.emacs
            # gui
            firefox

            # texlive
            (texlive.combine { inherit (texlive)
                collection-basic
                collection-bibtexextra
                collection-binextra
                collection-context
                collection-fontsextra
                collection-fontsrecommended
                collection-fontutils
                collection-formatsextra
                collection-humanities
                collection-langchinese
                collection-langcjk
                collection-langenglish
                collection-langjapanese
                collection-latex
                collection-latexextra
                collection-latexrecommended
                collection-luatex
                collection-mathscience
                collection-metapost
                collection-pictures
                collection-plaingeneric
                collection-pstricks
                collection-publishers
                collection-xetex;
            })
          ];

  };

  fonts.fonts = with pkgs; [
    ibm-plex
    inconsolata
    liberation_ttf
    noto-fonts
    noto-fonts-cjk
    roboto-mono
    source-han-sans
    source-han-serif
    source-han-mono
    terminus_font
  ];
}
