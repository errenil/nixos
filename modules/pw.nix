# power management module
{ pkgs, config, ... }:

{
  # List services that you want to enable:


  # power management
  services.tlp = {
    enable = true;
    settings = {
        "TLP_DEFAULT_MODE" = "BAT";
        "TLP_PERSISTENT_DEFAULT" = 0;
        "START_CHARGE_THRESH_BAT0" = 70;
        "STOP_CHARGE_THRESH_BAT0" =  80;
        "SOUND_POWER_SAVE_ON_BAT" = 1;
        "CPU_SCALING_GOVERNOR_ON_BAT" = "balance_power";
        "CPU_MIN_PERF_ON_BAT" = 0;
        "CPU_MAX_PERF_ON_BAT" = 75;
        "CPU_ENERGY_PERF_POLICY_ON_BAT" = "power";
        "CPU_BOOST_ON_AC" = 1;
        "CPU_BOOST_ON_BAT" = 0;
        "CPU_HWP_ON_BAT" = "balance_power";
        "DEVICES_TO_DISABLE_ON_STARTUP" = "bluetooth";
        "SCHED_POWERSAVE_ON_AC" = 0;
        "SCHED_POWERSAVE_ON_BAT" = 1;
        "NMI_WATCHDOG" = 0;
        "USB_AUTOSUSPEND" = 1;
        #"DEVICES_TO_DISABLE_ON_WIFI_CONNECT" = "wwan";
        #"DEVICES_TO_DISABLE_ON_LAN_CONNECT" = "wwan";
        #"DEVICES_TO_ENABLE_ON_STARTUP" = "wifi";
        "RUNTIME_PM_ON_BAT" = "auto";
    };	
  };
	
 # services.acpid = {
 #	enable = true;
 #	
 # };

  services.thermald = {
    enable = true;
  };

  services.acpid = {
    enable = true;
  };
}
