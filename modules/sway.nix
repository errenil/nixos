{ pkgs, config, ... }:
{
  programs.sway = {
    enable = true;
    wrapperFeatures.gtk = true;
    extraPackages = with pkgs; [
        alacritty
        swaylock
        swayidle
        wl-clipboard
        mako
        wofi
    ];
  };
  #xdg.portal.wlr.enable = true;
}
