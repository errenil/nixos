# Define a user account. Don't forget to set a password with ‘passwd’.
{ pkgs, config, ... }:

{
  users.users.errenil = {
    isNormalUser = true;
    name = "errenil";
    home = "/home/errenil";
    description = "Noel Errenil";
    shell = pkgs.zsh;
    extraGroups = [ "wheel" "video" ]; # Enable ‘sudo’ for the user.
    packages = with pkgs; [

	# clang/llvm
	clang_12
	llvmPackages_12.llvm


	# nodejs
	nodejs-16_x

	];
  };

  security.sudo.extraRules = [
    { groups = [ "wheel" ];
      commands = [ "ALL" ]; }
  ];
}
