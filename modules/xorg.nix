# Xorg services
{ pkgs, config, ... }:

{
  # Enable the X11 windowing system.
  services.xserver = {
    enable = true;
    autorun = false;
    videoDrivers = [ "modesetting" ];
    useGlamor = true;

    # Configure keymap in X11
    layout = "us";
    xkbOptions = "terminate:ctrl_alt_bksp,caps:escape";

    # WM
    displayManager = {
        startx.enable = true;
        defaultSession = "none+awesome";
    };
    windowManager.awesome = {
        enable = true;
    };

    # Enable touchpad support (enabled default in most desktopManager).
    libinput = {
        enable = true;
        touchpad = {
            clickMethod = "clickfinger";
            naturalScrolling = true;
            scrollMethod = "twofinger";
            tapping = true;
        };
    };
  };
}
