{ pkgs, config, ... }:

{
  programs.zsh = {
	enable = true;
	enableCompletion = true;
	autosuggestions = {
            enable = true;
            highlightStyle = "fg=7";
	};
	syntaxHighlighting = {
            enable = true;
            styles = {
                "alias" = "fg=yellow,bold";
            };
	};
	shellAliases = {
            "e" = "emacs -nw";
            "ec"= "emacsclient -nw";
            "mg"= "mg -n";
	};
	promptInit = ''
	if [ $(whoami) = 'errenil' ]; then
            PS1="%F{magenta}> %f"
            PS2="%F{yellow}>> %f"
            RPS1="%(?..%F{red}(%?%))%f %B%F{green}%3c %f%b"
	else
            PS1="%F{red}%n%f %~"
            PS2="%F{yellow}>> %f"
            RPS1="%(?..(%?%))"
	fi
	'';
  };
}
